const express = require('express')
const router = express.Router()

const CyberShopController = require('../controllers/CyberShopController')

router.get('/products', CyberShopController.getProducts)

module.exports = router