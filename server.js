const express = require('express')

const app = express()

const routerCyber = require('./routes/cybershop')

app.use('/api/cybershop-service', routerCyber)


module.exports = app