const http = require('http')
const https = require('https')
const app = require('./server')

const portHttp = 5000

http.createServer(app).listen(portHttp, () => console.info(`Server Http corriendo on port ${portHttp}!`))